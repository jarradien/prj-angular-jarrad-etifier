import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Module } from '../interfaces/formation.interface';

const API_MODULE="http://localhost:8082/api/module";

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  constructor(private http: HttpClient) { }

  getAllModule(): Observable<Module[]>{
    return this.http.get<Module[]>(API_MODULE);
  }

  getModuleById(id: string){
    return this.http.get<Module>(API_MODULE+'/'+id);
  }
}
