import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Filiere } from '../interfaces/formation.interface';

const API_FILIERE: string = "http://localhost:8082/api/filiere";

@Injectable({
  providedIn: 'root'
})
export class FiliereService {

  constructor(private http: HttpClient) { }

  getAllFiliere(): Observable<Filiere[]>{
    return this.http.get<Filiere[]>(API_FILIERE);
  }

  postFiliere(filiereToPost:  Filiere) {
    return this.http.post(API_FILIERE, filiereToPost);
  }

  getFiliereById(id: string){
    return this.http.get<Filiere>(API_FILIERE+'/'+id);
  }

  putFiliere(filiereToPut: Filiere){
    return this.http.put(API_FILIERE, filiereToPut);
  }

  putDeleteFiliere(filiereToDelete: Filiere){
    return this.http.put(API_FILIERE+'/'+ filiereToDelete.id, filiereToDelete );
  }
}
