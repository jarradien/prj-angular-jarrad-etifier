export interface Filiere{
    id?: number;
    libelle: string;
    modules?: Module[];
    stagiaires?: Personne[];

}

export interface Module{
    id?: number;
    libelle: string;
    dateDebut: string; 
    dateFin: string; 
    formateur?: Personne;
    filiere?: Filiere;
}

export interface Personne{
    id?: number;
    modules?: [];
    nom: string;
    prenom: string;
    type: string;
}