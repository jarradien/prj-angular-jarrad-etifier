import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MenuComponent } from './menu/menu.component';
import { FiliereCrudComponent } from './filiere-crud/filiere-crud.component';
import { FiliereListComponent } from './filiere-list/filiere-list.component';
import { FiliereFormComponent } from './filiere-form/filiere-form.component';
import { FiliereInfosComponent } from './filiere-infos/filiere-infos.component';
import { ModuleCrudComponent } from './module-crud/module-crud.component';
import { ModuleListComponent } from './module-list/module-list.component';
import { ModuleFormComponent } from './module-form/module-form.component';






@NgModule({
  declarations: [
    MenuComponent,
    FiliereCrudComponent,
    FiliereListComponent,
    FiliereFormComponent,
    FiliereInfosComponent,
    ModuleCrudComponent,
    ModuleListComponent,
    ModuleFormComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: 'filiere', component: FiliereCrudComponent},
      { path: 'filiere/list', component: FiliereListComponent},
      { path: 'filiere/form/:id', component: FiliereFormComponent},
      { path: 'filiere/form', component: FiliereFormComponent},
      { path: 'filiere/infos/:id', component: FiliereInfosComponent},
      { path: 'module', component: ModuleCrudComponent},
      { path: 'module/list', component: ModuleListComponent},
      { path: 'module/form', component: ModuleFormComponent}
    ])
  ],
  exports: [
    MenuComponent
  ]
})
export class FormationModule { }
