import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Filiere } from '../interfaces/formation.interface';
import { FiliereService } from '../services/filiere.service';

@Component({
  selector: 'filiere-infos',
  templateUrl: './filiere-infos.component.html',
  styleUrls: ['./filiere-infos.component.css']
})
export class FiliereInfosComponent implements OnInit {

  urlFiliereId: string | null ='';

  filiere: Filiere | null =null;

  constructor(private filiereService: FiliereService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.urlFiliereId = this.route.snapshot.paramMap.get('id');

    if(this.urlFiliereId != null && this.urlFiliereId != ''){
      this.filiereService.getFiliereById(this.urlFiliereId).subscribe(res => this.filiere = res );
    }
  }

}
