import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiliereInfosComponent } from './filiere-infos.component';

describe('FiliereInfosComponent', () => {
  let component: FiliereInfosComponent;
  let fixture: ComponentFixture<FiliereInfosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiliereInfosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiliereInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
