import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Filiere } from '../interfaces/formation.interface';
import { FiliereService } from '../services/filiere.service';

@Component({
  selector: 'filiere-form',
  templateUrl: './filiere-form.component.html',
  styleUrls: ['./filiere-form.component.css']
})
export class FiliereFormComponent implements OnInit {

  urlFiliereId: string | null ='';

  filiere: Filiere = {
    libelle: '',
    stagiaires: []
  }

  constructor(private filiereService: FiliereService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.urlFiliereId = this.route.snapshot.paramMap.get('id');

    if(this.urlFiliereId != null && this.urlFiliereId != ''){
      this.filiereService.getFiliereById(this.urlFiliereId).subscribe(res => this.filiere = res );
    }
  }

  handleSubmit() {

    if(this.urlFiliereId==''){
      this.filiereService.postFiliere(this.filiere).subscribe();
      
    }else{
      this.filiereService.putFiliere(this.filiere).subscribe();
    }

    this.filiere.libelle = '';
    window.location.href = "/filiere/list";
    
  }

}
