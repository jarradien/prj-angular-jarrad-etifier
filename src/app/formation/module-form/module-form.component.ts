import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Filiere, Module } from '../interfaces/formation.interface';
import { ModuleService } from '../services/module.service';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl} 
  from '@angular/forms';
import { FiliereService } from '../services/filiere.service';

const regexDate = /^\d{4}-?((((0[13578])|(1[02]))-?(([0-2][0-9])|(3[01])))|(((0[469])|(11))-?(([0-2][0-9])|(30)))|(02-?[0-2][0-9]))$/g;

function customValidator(): ValidatorFn{
  return (control: AbstractControl) => 
    control.value === 'custom' ? null : { bad: control.value }
}

@Component({
  selector: 'module-form',
  templateUrl: './module-form.component.html',
  styleUrls: ['./module-form.component.css']
})
export class ModuleFormComponent implements OnInit {

  urlModuleId: string | null ='';

  moduleForm: FormGroup = new FormGroup({

    libelle: new FormControl('', Validators.required),

    dateDebut: new FormControl('', [
      Validators.pattern(regexDate),
      Validators.required
    ]),
    
    dateFin: new FormControl('', [
      Validators.pattern(regexDate),
      Validators.required
    ]),

    filiere: new FormControl()

    
  });

  module: Module = {
    libelle: '',
    dateDebut: '',
    dateFin: '',
    filiere:  {} as Filiere 
  }

  filieres: Filiere[] = [];

  constructor(private filiereService: FiliereService, private moduleService: ModuleService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.urlModuleId = this.route.snapshot.paramMap.get('id');
    this.filiereService.getAllFiliere().subscribe(res => this.filieres=res);

    if(this.urlModuleId != null && this.urlModuleId != ''){
      this.moduleService.getModuleById(this.urlModuleId).subscribe((res: Module) => this.module = res );
    }
  }

  handleSubmit() {
    
  }

  handleChange(event: any){
    console.log(event.target.value);
  }

}
