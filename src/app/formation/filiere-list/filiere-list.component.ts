import { Component, OnInit } from '@angular/core';
import { Filiere } from '../interfaces/formation.interface';
import { FiliereService } from '../services/filiere.service';

@Component({
  selector: 'filiere-list',
  templateUrl: './filiere-list.component.html',
  styleUrls: ['./filiere-list.component.css']
})
export class FiliereListComponent implements OnInit {

  filieres: Filiere[] = [];

  constructor(private filiereService: FiliereService ) { }

  ngOnInit(): void {
    this.filiereService.getAllFiliere().subscribe((res: Filiere[]) => {
      this.filieres = res;
    });
  }

  handleDelete(filiere: Filiere){
  
    this.filiereService.putDeleteFiliere(filiere).subscribe();
    window.location.reload();

  }



}
