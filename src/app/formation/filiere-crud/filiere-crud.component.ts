import { Component, OnInit } from '@angular/core';
import { FiliereService } from '../services/filiere.service';

@Component({
  selector: 'filiere-crud',
  templateUrl: './filiere-crud.component.html',
  styleUrls: ['./filiere-crud.component.css']
})
export class FiliereCrudComponent implements OnInit {

  constructor(private filiereService: FiliereService) { }

  ngOnInit(): void {
    // this.filiereService.getAllFiliere().subscribe(res => console.log(res));
  }

}
