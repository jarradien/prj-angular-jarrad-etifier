import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiliereCrudComponent } from './filiere-crud.component';

describe('FiliereCrudComponent', () => {
  let component: FiliereCrudComponent;
  let fixture: ComponentFixture<FiliereCrudComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiliereCrudComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiliereCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
