import { Component, OnInit } from '@angular/core';
import { Module } from '../interfaces/formation.interface';
import { ModuleService } from '../services/module.service';

@Component({
  selector: 'module-list',
  templateUrl: './module-list.component.html',
  styleUrls: ['./module-list.component.css']
})
export class ModuleListComponent implements OnInit {

  modules: Module[] = [];

  constructor(private moduleService: ModuleService ) { }

  ngOnInit(): void {
    this.moduleService.getAllModule().subscribe((res: Module[]) => {
      this.modules = res;
    });
  }
}
